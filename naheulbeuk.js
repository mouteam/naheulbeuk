import { Naheulbeuk } from "./module/config.js";
import NaheulbeukCharacterActorSheet from "./module/actor/NaheulbeukCharacterActorSheet.js";
import NaheulbeukItemSheet from "./module/item/NaheulbeukItemSheet.js";
import {preloadHandlebarsTemplates} from "./module/templates.js";

Hooks.once("init", function () {
    console.log('Naheulbeuk | Initialising the Naheulbeuk game system.')

    CONFIG.Naheulbeuk = Naheulbeuk;

    Actors.unregisterSheet("core", ActorSheet);
    Actors.registerSheet("naheulbeuk", NaheulbeukCharacterActorSheet, {
        types: ["character"],
        makeDefault: true,
        label: "naheulbeuk.sheet.defaultCharacter"
    });

    Items.unregisterSheet("core", ItemSheet);
    Items.registerSheet("naheulbeuk", NaheulbeukItemSheet, {
        makeDefault: true,
        label: "naheulbeuk.sheet.defaultClassItem"
    })

    preloadHandlebarsTemplates();
});