export default class NaheulbeukItemSheet extends ItemSheet {
    constructor(...args) {
        super(...args);

        // Expand the default size of the class sheet
        //if (this.object.data.type === "class") {
        //    this.options.width = this.position.width = 600;
        //    this.options.height = this.position.height = 680;
        //}
    }

    /** @override */
    static get defaultOptions() {
        return mergeObject(super.defaultOptions, {
            width: 560,
            height: 400,
            classes: ["naheulbeuk", "sheet", "item"],
            resizable: true,
            scrollY: [".tab.details"],
            tabs: [{navSelector: ".tabs", contentSelector: ".sheet-body", initial: "description"}]
        });
    }

    /** @override */
    get template() {
        return `systems/naheulbeuk/templates/items/${this.item.data.type}.hbs`;
    }

    /** @override */
    getData(options) {
        const data = super.getData(options);

        data.config = CONFIG.Naheulbeuk;

        console.log(data);
        return data;
    }
}