export default class NaheulbeukCharacterActorSheet extends ActorSheet {
    /** @override */
    static get defaultOptions() {
        return mergeObject(super.defaultOptions, {
            classes: ["naheulbeuk", "sheet", "actor", "character"],
            width: 970,
            height: 750,
        });
    }

    /** @override */
    get template() {
        return `systems/naheulbeuk/templates/actors/character-sheet.hbs`;
    }

    /** @override */
    getData(options) {
        const data = super.getData(options);

        data.config = CONFIG.Naheulbeuk;

        return data;
    }
}