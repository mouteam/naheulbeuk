export const Naheulbeuk = {};

Naheulbeuk.abilities = {
    "cou": "naheulbeuk.AbilityCou",
    "int": "naheulbeuk.AbilityInt",
    "cha": "naheulbeuk.AbilityCha",
    "dex": "naheulbeuk.AbilityDex",
    "str": "naheulbeuk.AbilityStr"
};
